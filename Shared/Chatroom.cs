﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bomberman.Shared
{
    /// <summary>
    /// The 'Mediator' abstract public class
    /// </summary>
    abstract public class AbstractChatroom
    {
        public abstract void Register(Player participant);
        public abstract void Apply(string from, string message);
    }

    /// <summary>
    /// The 'AbstractColleague' public class
    /// </summary>
    public partial class Player
    {
        // Constructor
        public Player(string name)
        {
            this.Name = name;
        }

        public Chatroom Chatroom { set; get; }

        // Sends message to given participant
        public void Send(string message)
        {
            Chatroom.Apply(Name, message);
        }

        // Receives message from given participant
        public virtual void Receive(string from, string message)
        {
            Console.WriteLine("{0} to {1}: '{2}'",
              from, Name, message);
        }
    }

    /// <summary>
    /// The 'ConcreteMediator' class
    /// </summary>
    public class Chatroom : AbstractChatroom
    {
        private Dictionary<string, Player> _participants = new Dictionary<string, Player>();
        public Queue<string> Chat = new Queue<string>();

        public override void Register(Player participant)
        {
            if (!_participants.ContainsValue(participant))
            {
                _participants[participant.Name] = participant;
            }

            participant.Chatroom = this;
        }

        public override void Apply(string from, string message)
        {
            Chat.Enqueue(from + ": " + message);
        }
    }

    /// <summary>
    /// A 'ConcreteColleague' public class
    /// </summary>
    public class Teammate : Player
    {
        // Constructor
        public Teammate(string name)
          : base(name)
        {
        }

        public override void Receive(string from, string message)
        {
            Console.Write("To a team mate: ");
            base.Receive(from, message);
        }
    }

    /// <summary>
    /// A 'ConcreteColleague' public class
    /// </summary>
    public class Enemy : Player
    {
        // Constructor
        public Enemy(string name)
          : base(name)
        {
        }

        public override void Receive(string from, string message)
        {
            Console.Write("To a enemy: ");
            base.Receive(from, message);
        }
    }
}