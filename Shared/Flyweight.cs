﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bomberman.Shared
{

    public class FlyweightBombsFactory
    {
        Dictionary<char, BombBuildStrategy> strategies = new Dictionary<char, BombBuildStrategy>();
        public BombBuildStrategy GetBombBuildStrategy(char key)
        {
            // Uses "lazy initialization"

            BombBuildStrategy character = null;
            if (strategies.ContainsKey(key))
            {
                character = strategies[key];
            }
            else
            {
                switch (key)
                {
                    case 'A': character = new BombBuildStrategyA(); break;
                    case 'B': character = new BombBuildStrategyB(); break;
                    case 'C': character = new BombBuildStrategyC(); break;
                }
                strategies.Add(key, character);
            }
            return character;
        }
    }
}
