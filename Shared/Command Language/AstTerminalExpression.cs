﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bomberman.Shared.Command_Language
{
    public class AstTerminalExpression : AstExpression
    {
        private string data;
        public AstTerminalExpression(string expressionData)
        {
            data = expressionData;
        }

        public override int EvaluateInt()
        {
            if (int.TryParse(data, out int i))
            {
                return i;
            }
            throw new Exception("Integer Parse error");
        }
    }
}
