﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bomberman.Shared.Command_Language
{
    public class AstOperatorExpression : AstExpression
    {
        string op;
        public AstOperatorExpression(string op)
        {
            this.op = op;
        }

        public override int EvaluateInt()
        {
            int p1 = ((leaf as AstComposite).Children[0] as AstTerminalExpression).EvaluateInt();
            int p2 = ((leaf as AstComposite).Children[1] as AstTerminalExpression).EvaluateInt();

            if (op.Contains("*"))
            {
                return p1 * p2;
            }

            if (op.Contains("/"))
            {
                return p1 / p2;
            }

            if (op.Contains("+"))
            {
                return p1 + p2;
            }

            if (op.Contains("-"))
            {
                return p1 - p2;
            }

            throw new Exception("Invalid operation");
        }
    }
}
