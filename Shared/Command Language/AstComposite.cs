﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bomberman.Shared.Command_Language
{
    public class AstComposite : AstComponent
    {
        public AstComponent[] Children { get; set; }
    }
}
