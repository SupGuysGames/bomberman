﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bomberman.Shared.Command_Language
{
    public class AstComponent
    {
        protected AstComponent leaf;

        public AstComponent(string data)
        {
            data = data.Trim();

            if (data.StartsWith("!{") && data.EndsWith("}"))
            {
                leaf = new AstComposite
                {
                    Children = data.Substring(2, data.Length - 3).Split(";", StringSplitOptions.RemoveEmptyEntries)
                    .Select(r => CreateCommand(r)).ToArray()
                };
            }
            else if(data.StartsWith("!"))
            {
                leaf = CreateCommand(data.Substring(1, data.Length - 1));
            }
            else
            {
                leaf = new AstSendMessage(data);
            }
        }

        private static AstComponent CreateCommand(string data)
        {
            data = data.Trim();
            if (data.StartsWith("Move"))
            {
                string[] pars = data.Substring(4, data.Length - 4).Trim().Split(",", StringSplitOptions.RemoveEmptyEntries);

                if (pars.Length != 3) throw new Exception("Invalid parameter count for move player");

                return new AstMovePlayer { leaf = new AstComposite { Children = pars.Select(p => CreateParameter(p)).ToArray() } };
            }
            else if(data.StartsWith("Spawn"))
            {
                string[] pars = data.Substring(5, data.Length - 5).Trim().Split(",", StringSplitOptions.RemoveEmptyEntries);

                if (pars.Length != 2) throw new Exception("Invalid parameter count for spawn box");

                return new AstSpawnBox { leaf = new AstComposite { Children = pars.Select(p => CreateParameter(p)).ToArray() } };
            }
            else
            {
                throw new Exception("Unrecognized command");
            }
        }

        private static AstComponent CreateParameter(string data) {
            return AstExpression.Create(data);
        }

        public virtual void Interpret(CommandContext context)
        {
            context.ResultMessage = "Command finished successfully.";

            if (leaf is AstComposite)
            {
                foreach(AstComponent comp in (leaf as AstComposite).Children)
                {
                    comp.Interpret(context);
                }
            }
            else if (leaf != null)
            {
                leaf.Interpret(context);
            }
        }

        protected AstComponent() { }
    }
}
