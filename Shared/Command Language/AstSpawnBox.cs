﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bomberman.Shared.Command_Language
{
    public class AstSpawnBox : AstComponent
    {
        public override void Interpret(CommandContext context)
        {
            try
            {
                int x = ((leaf as AstComposite).Children[0] as AstExpression).EvaluateInt();
                int y = ((leaf as AstComposite).Children[1] as AstExpression).EvaluateInt();

                context.GameState.Apply(new GameObject
                {
                    Position = new Vector2d(x, y),
                    type = GameObject.Type.Wall
                });
            }
            catch (Exception ex)
            {
                context.ResultMessage = ex.Message;
            }
        }
    }
}
