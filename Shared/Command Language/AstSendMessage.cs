﻿using System;
using System.Collections.Generic;
using System.Text;
using Bomberman.Shared.Command_Language.LanguageFiltering;

namespace Bomberman.Shared.Command_Language
{
    public class AstSendMessage : AstComponent
    {
        private string message;

        public AstSendMessage(string message)
        {
            this.message = message;
        }

        public override void Interpret(CommandContext context)
        {
            LanguageFilterHandler h1 = new ParametersHandler();
            LanguageFilterHandler h2 = new BadWordsHandler();
            LanguageFilterHandler h3 = new TextEffectsHandler();
            LanguageFilterHandler h4 = new MaxSizeHandler();

            h1.SetNext(h2);
            h2.SetNext(h3);
            h3.SetNext(h4);

            context.ResultMessage = h1.Handle(message, context.GameState);
        }
    }
}
