﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bomberman.Shared.Command_Language.LanguageFiltering
{
    public abstract class LanguageFilterHandler
    {
        protected LanguageFilterHandler next;

        public abstract string Handle(string text, GameState gameState);
        public void SetNext(LanguageFilterHandler handler)
        {
            next = handler;
        }
    }
}
