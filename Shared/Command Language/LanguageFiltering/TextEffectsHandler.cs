﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bomberman.Shared.Command_Language.LanguageFiltering
{
    public class TextEffectsHandler : LanguageFilterHandler
    {
        public override string Handle(string text, GameState gameState)
        {
            if (text.Contains("@~@"))
            {
                text = text.Replace("@~@", "");
                string newText = "";

                bool up = false;
                for (int i = 0; i < text.Length; i++)
                {
                    newText += up ? char.ToLower(text[i]) : char.ToUpper(text[i]);
                    up = !up;
                }
                text = newText;
            }

            if (text.Contains("@UP@"))
            {
                text = text.Replace("@UP@", "").ToUpper();
            }

            if (text.Contains("@LOW@"))
            {
                text = text.Replace("@LOW@", "").ToLower();
            }

            if (next != null) return next.Handle(text, gameState);

            return text;
        }
    }
}
