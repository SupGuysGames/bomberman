﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bomberman.Shared.Command_Language.LanguageFiltering
{
    public class MaxSizeHandler : LanguageFilterHandler
    {
        public override string Handle(string text, GameState gameState)
        {
            if(text.Length > 40)
            {
                text = text.Substring(0, 37) + "...";
            }
            if (next != null) return next.Handle(text, gameState);

            return text;
        }
    }
}
