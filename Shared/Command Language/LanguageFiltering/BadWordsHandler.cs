﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bomberman.Shared.Command_Language.LanguageFiltering
{
    public class BadWordsHandler : LanguageFilterHandler
    {
        public override string Handle(string text, GameState gameState)
        {
            foreach (string word in new string[] { "n-word", "idiot", "hitler" })
            {
                text = text.Replace(word, "-_-", StringComparison.OrdinalIgnoreCase);
            }

            if (next != null) return next.Handle(text, gameState);

            return text;
        }
    }
}
