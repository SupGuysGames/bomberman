﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bomberman.Shared.Command_Language.LanguageFiltering
{
    public class ParametersHandler : LanguageFilterHandler
    {
        public override string Handle(string text, GameState gameState)
        {
            text = text.Replace("@:playerCount", gameState.gameObjects.Values.Count(g => g.type == GameObject.Type.Player).ToString());
            text = text.Replace("@:mapSize",
                gameState.gameObjects.Values.Where(g => g.type == GameObject.Type.Wall).Max(g => g.Position.x) + ":" +
                gameState.gameObjects.Values.Where(g => g.type == GameObject.Type.Wall).Max(g => g.Position.y));

            if (next != null) return next.Handle(text, gameState);

            return text;
        }
    }
}
