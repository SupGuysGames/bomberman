﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bomberman.Shared.Command_Language
{
    public abstract class AstExpression : AstComponent
    {
        public abstract int EvaluateInt();

        public static AstExpression Create(string rawData)
        {
            rawData = rawData.Trim();
            string op = null;

            if (rawData.Contains("*")) op = "*";
            if (rawData.Contains("/")) op = "/";
            if (rawData.Contains("+")) op = "+";
            if (rawData.Contains("-")) op = "-";

            if (op == null) return new AstTerminalExpression(rawData);

            string[] pars = rawData.Split(op, StringSplitOptions.RemoveEmptyEntries);
            if (pars.Length != 2)
            {
                throw new Exception("Invalid parameter count in expression: " + pars.Length);
            }

            else
            {
                return new AstOperatorExpression(op)
                {
                    leaf = new AstComposite { Children = pars.Select(p => new AstTerminalExpression(p)).ToArray() }
                };
            }
        }
    }
}
