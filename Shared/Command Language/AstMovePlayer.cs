﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bomberman.Shared.Command_Language
{
    public class AstMovePlayer : AstComponent
    {
        public override void Interpret(CommandContext context)
        {
            try
            {
                int playerId = ((leaf as AstComposite).Children[0] as AstExpression).EvaluateInt();
                int x = ((leaf as AstComposite).Children[1] as AstExpression).EvaluateInt();
                int y = ((leaf as AstComposite).Children[2] as AstExpression).EvaluateInt();

                GameObject player = context.GameState.gameObjects.Values.FirstOrDefault(g => g.InstanceID == playerId && g is Player);

                if (player == null) throw new Exception("Player not found");

                player.Position = new Vector2d(x, y);

                context.ResultMessage = "Player Moved Successfully";
            } catch(Exception ex)
            {
                context.ResultMessage = ex.Message;
            }
        }
    }
}
