﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bomberman.Shared.Command_Language
{
    public class CommandContext
    {
        public GameState GameState { get; set; }
        public Chatroom Chatroom { get; set; }
        public string Invoker { get; set; }
        public string ResultMessage { get; set; }
    }
}
