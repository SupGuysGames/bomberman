﻿using Bomberman.Shared;
using Bomberman.Shared.Command_Language;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace Bomberman.Server.Hubs
{
    public class GameObjectHub : Hub
    {
        private OurGameObjectFactory factory = new OurGameObjectFactory();

        public async Task SendNetworkObjects(string gameObjectData)
        {
            NetworkObject[] netObjects = JsonConvert.DeserializeObject<NetworkObject[]>(gameObjectData);
            foreach (NetworkObject netObject in netObjects)
            {
                // Add gameobject to gamestate
                Startup.GameState.Apply(factory.Create(netObject));
            }

            NetworkObject[] toSend = Startup.GameState.gameObjects.Values.ToList()
                .Where(g => g.type != GameObject.Type.Ground && g.type != GameObject.Type.Wall) // dont sync ground and walls
                .Select(g => new NetworkObject
                {
                    Type = g.type,
                    InstanceID = g.InstanceID,
                    OwnerID = g.UserID,
                    Data = JsonConvert.SerializeObject(g)
                }).ToArray();

            await Clients.All.SendAsync("ReceiveNetworkObjects", JsonConvert.SerializeObject(toSend));
        }

        private async Task SendAllObjects()
        {
            NetworkObject[] toSend = Startup.GameState.gameObjects.Values.ToList()
                .Select(g => new NetworkObject
                {
                    Type = g.type,
                    InstanceID = g.InstanceID,
                    OwnerID = g.UserID,
                    Data = JsonConvert.SerializeObject(g)
                }).ToArray();

            await Clients.All.SendAsync("ReceiveNetworkObjects", JsonConvert.SerializeObject(toSend));
        }

        public async Task SendChatMessage(string from, string message)
        {
            CommandContext commandContext = new CommandContext
            {
                GameState = Startup.GameState,
                Chatroom = Startup.Chatroom,
                Invoker = from
            };

            try
            {
                AstComponent root = new AstComponent(message);
                root.Interpret(commandContext);
            }
            catch (Exception ex)
            {
                commandContext.ResultMessage = "ERROR: Client " + from + " : " + ex.Message;
            }

            Startup.Chatroom.Apply(from, commandContext.ResultMessage);
            await Clients.All.SendAsync("ReceiveChatMessage", from, commandContext.ResultMessage);
            await SendAllObjects();
        }
    }
}
